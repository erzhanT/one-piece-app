import React from 'react';
import './Cell.css';

const Cell = (props) => {
    let classNames = "Cell";
    let content = null;

    if (props.ring === true && props.open === true) {
        content = "O"
    }

    if (props.open === true) {
        classNames = "Cell Open"
    }

    return (
        <div onClick={props.onClick} className={classNames}>
            {content}
        </div>
    );
};

export default Cell;