import React, {useState} from 'react';
import './Field.css';
import Cell from "../../components/Cell/Cell";

const Field = () => {

    const [cells, setCells] = useState([]);
    const [counter, setCounter] = useState(0);


    const start = () => {
        const array = [];

        for (let i = 0; i < 36; i++) {
            const newCell = {
                hasRing: false,
                isOpen: false,
                id: i
            };

            array.push(newCell);
        }
        const randIndex = Math.floor(Math.random() * array.length);
        const randomCell = array[randIndex];
        randomCell.hasRing = true;

        setCells(array);
        setCounter(0)

    };

    const onCellClick = (targetId) => {
        const cellsCopy = [...cells];

        const index = cellsCopy.findIndex(el => {
            return el.id === targetId;
        });

        if (cellsCopy[index].isOpen === false) {
            setCounter(counter + 1);
        }


        cellsCopy[index].isOpen = true;

        setCells(cellsCopy);

    };

    // const elems = [];
    //
    // for (let i = 0; i < cells.length; i++) {
    //     const cell = cells[i]; // {id: q3, hasRing: false .....}
    //
    //     const component = (
    //         <Cell
    //             key={cell.id}
    //             ring={cell.hasRing}
    //             open={cell.isOpen}
    //             onClick={() => onCellClick(cell.id)}
    //         />
    //     )
    //     elems.push(component)
    // }

    const elems = cells.map(cell => {
        return (
            <Cell
                key={cell.id}
                ring={cell.hasRing}
                open={cell.isOpen}
                onClick={() => onCellClick(cell.id)}
            />
        )
    });

    return (
        <React.Fragment>
            <button onClick={start}>Start Game</button>
            <div className="Field">
                {elems}
            </div>
            <div className="Counter">
                {counter + ' : tries'}
            </div>
        </React.Fragment>
    );
};

export default Field;